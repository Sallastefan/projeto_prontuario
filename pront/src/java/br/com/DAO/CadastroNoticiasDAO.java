
package br.com.DAO;

import br.com.controle.Noticias;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Calendar;


public class CadastroNoticiasDAO extends DAO{
     public void inserir(Noticias ms) throws Exception {
    try {
    abrirBanco();
    String query = "INSERT INTO PRONTUARIO (nome,registro_agenda,historico,receituario,exames)  "
            + "values(?,?,?,?,?)";
    pst=(PreparedStatement) con.prepareStatement(query);
    pst.setString(1, ms.getNome());
    pst.setDate(2, new java.sql.Date
    (Calendar.getInstance().getTimeInMillis()));
    pst.setString(3,ms.getHistorico());
    pst.setString(4,ms.getReceituario());
    pst.setString(5,ms.getExames());
    
     
    pst.execute();
    fecharBanco();
    } catch (Exception e) {
        System.out.println("Erro " + e.getMessage());
    }
    }

	public ArrayList<Noticias> pesquisarTudo () throws Exception {
       ArrayList<Noticias> listantc = new ArrayList<Noticias>();
         try{
         abrirBanco();  
         String query = "select  registro,nome,historico,receituario,exames, DATE_FORMAT(registro_agenda,'%d/%m/%Y') AS registro_agenda FROM PRONTUARIO order by registro";
         //Problemaa da data resolvido aqui com recuroso do sql
         pst = con.prepareStatement(query);
         ResultSet rs = pst.executeQuery();
         Noticias ntcben ;
         while (rs.next()){ 
           ntcben = new Noticias();
           ntcben.setRegistro(rs.getInt("registro"));
           ntcben.setNome(rs.getString("nome"));
          ntcben.setHistorico(rs.getString("historico"));
          ntcben.setReceituario(rs.getString("receituario"));
          ntcben.setExames(rs.getString("exames"));
          
           ntcben.setData_agenda(rs.getString("registro_agenda"));
           listantc.add(ntcben); 
         } 
         fecharBanco();
       }catch (Exception e){
           System.out.println("Erro " + e.getMessage());
     } 
       return listantc;
     }

        
         public void deletar(Noticias nt) throws Exception{
         abrirBanco();
         String query = "delete FROM PRONTUARIO where registro=?";
         pst=(PreparedStatement) con.prepareStatement(query);
         pst.setInt(1, nt.getRegistro());
         pst.execute();
        fecharBanco();   
     }
         
         public void alterar (Noticias nt) throws Exception {
		try {
    abrirBanco();
    String query = "UPDATE PRONTUARIO SET nome=?,registro_agenda=?, historico=?,receituario=?,exames=? WHERE registro=?;";
    pst = con.prepareStatement(query);
    pst.setString(1, nt.getNome());
    pst.setDate(2, new java.sql.Date (Calendar.getInstance().getTimeInMillis()));
    pst.setString(3, nt.getHistorico());
    pst.setString(4, nt.getReceituario());
    pst.setString(5, nt.getExames());
    
    pst.setInt(6, nt.getRegistro());
    pst.executeUpdate();
    fecharBanco();
			
    } catch (Exception e) {
            System.out.println("Erro " + e.getMessage());
    }
	}

      public Noticias pesquisar(int id) throws Exception {
    try {
            Noticias nt = new Noticias();
            System.out.println(" Chegou no pesquisar registo" + id);
            abrirBanco();
            String query = "select * from PRONTUARIO where registro=?";
            pst = con.prepareStatement(query);
            pst.setInt(1, id);
            ResultSet rs = pst.executeQuery();
            if (rs.next()) {
                nt.setRegistro(rs.getInt("registro"));
                nt.setNome(rs.getString("nome"));
                nt.setData_agenda(rs.getString("registro_agenda"));
                nt.setHistorico(rs.getString("historico"));
                nt.setReceituario(rs.getString("receituario"));
                nt.setExames(rs.getString("exames"));
              
                
                return nt;
            }
            fecharBanco();
    } catch (Exception e) {
            System.out.println("Erro " + e.getMessage());
    }
    return null;
	}   
         
         
}