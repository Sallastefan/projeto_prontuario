
package br.com.bean;

import br.com.DAO.CadastroNoticiasDAO;
import br.com.controle.Noticias;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ServletNoticias extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
          
          String snome= request.getParameter("nome");//nome do paciente
          String sdata= request.getParameter("data_agenda");//data da consulta
          String shistorico= request.getParameter("historico");//historico do paciente
          String sreceituario= request.getParameter("receituario");//receituario do paciente
          String sexames= request.getParameter("exames");//exames
          
           
            
           Noticias  ntcs = new Noticias();//instanciando o objeto controle
            
            ntcs.setNome(snome);//seteando o texto nome no objeto controle
            ntcs.setData_agenda(sdata);
            ntcs.setHistorico(shistorico);
            ntcs.setReceituario(sreceituario);
            ntcs.setExames(sexames);
            
             //System.out.println(""+svalor);
	       CadastroNoticiasDAO dao = new CadastroNoticiasDAO();//instanciando o objeto dao para usar a parte de SQL
               try {
                dao.inserir(ntcs);
                request.setAttribute("nome", ntcs.getNome());
                request.setAttribute("data_agenda", ntcs.getData_agenda());
                request.setAttribute("historico", ntcs.getHistorico());
                request.setAttribute("receituario", ntcs.getReceituario());
                request.setAttribute("exames", ntcs.getExames());
                
                RequestDispatcher rd = 
                request.getRequestDispatcher("sucesso.jsp");
                rd.forward(request, response);}
           catch(Exception e){
                   
                   }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
