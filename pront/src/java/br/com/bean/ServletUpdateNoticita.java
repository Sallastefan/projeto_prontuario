
package br.com.bean;

import br.com.DAO.CadastroNoticiasDAO;
import br.com.controle.Noticias;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


public class ServletUpdateNoticita extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            
            Noticias up = new Noticias();
            int registro = Integer.parseInt(request.getParameter("registro"));
            String nome = request.getParameter("nome_novo");
            String data_agenda = request.getParameter("Data_agenda_novo");
            String historico = request.getParameter("historico_novo");
            String receituario = request.getParameter("receituario_novo");
            String exames = request.getParameter("exames_novo");
            
           
            out.println("variavel"+registro);
            out.println("variavel"+nome);
            out.println("variavel"+data_agenda);
            out.println("variavel"+historico);
            out.println("variavel"+receituario);
            out.println("variavel"+exames);
            
            try {
                   up.setRegistro(registro);
                   up.setNome(nome);
                   up.setData_agenda(data_agenda);
                   up.setHistorico(historico);
                   up.setReceituario(receituario);//setando os novos valores no objeto controle
                   up.setExames(exames);
                   CadastroNoticiasDAO dao = new CadastroNoticiasDAO();
                   dao.alterar(up);
                   request.setAttribute("Nome", "Dados atualizados com sucesso!!");
                   RequestDispatcher rd = 
                    request.getRequestDispatcher("lista.jsp");
                    rd.forward(request, response);}
               catch(Exception e){
                    out.println(e);
                       }
        
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
