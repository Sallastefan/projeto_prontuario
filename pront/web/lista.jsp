

<%@page import="br.com.controle.Noticias"%>
<%@page import="java.util.ArrayList"%>
<%@page import="br.com.DAO.CadastroNoticiasDAO"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
        <!-- Linha para utilizar o bootstrap -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
        <!-- Linha para utilizar o JavaScript -->
       <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
       
        </head>
    <body>
        <%@include file="menu.jsp" %> 
        <table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">registro</th>
      <th scope="col">nome</th>
      <th scope="col">dataagend</th>
      <th scope="col">historico</th>
      <th scope="col">receituario</th>
      <th scope="col">exames</th>

    </tr>
  </thead>
  
  
        <%
          String vregistro="";
          String vnome="";
          String vdataagenda="";
          String vhistorico="";
          String vreceituario="";
          String vexames="";

          String ac = (String) request.getAttribute("relatorio");
         
         CadastroNoticiasDAO cnd = new CadastroNoticiasDAO(); 
             Noticias n =  new Noticias();
            
             ArrayList<Noticias> nt = cnd.pesquisarTudo();  
             
            for (int i = 0; i < nt.size(); i++) {                
                n = nt.get(i);
              vregistro= String.valueOf(n.getRegistro());
              vnome = String.valueOf(n.getNome());
             vdataagenda = String.valueOf(n.getData_agenda());
              vhistorico = String.valueOf(n.getHistorico());
              vreceituario = String.valueOf(n.getReceituario());
              vexames = String.valueOf(n.getExames());

               %>
              
            <tr>
              <th scope="row"><%=vregistro%></th>
              <td><%=vnome%></td>
              <td><%=vdataagenda%></td>            
              <td><%=vhistorico%></td>
              <td><%=vreceituario%></td>
              <td><%=vexames%></td>
            
              <td><a href="alterar?registro=<%=vregistro%>" >Alterar</a></td>
              <td>
                   <a href="del?registro=<%=vregistro%>" onclick="return confirm('Confirma exclusão do registro <%=vnome%>?')">excluir </a>   
                 
              </td>
            </tr>
          
            <%} 
        %>
         
        </table>
</html>