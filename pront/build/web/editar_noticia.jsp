
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Alterar Cadastro</title> 
       
    </head>
    <%  
        String vregistro="";
          String vnome="";
          String vdata_agenda="";
          String vhistorico="";
          String vreceituario="";
          String vexames="";


         vregistro = (String) request.getAttribute("ntRegistro").toString();
         vnome    = (String) request.getAttribute("ntNome").toString();
         vdata_agenda    = (String) request.getAttribute("ntData_agenda").toString();
         vhistorico = (String) request.getAttribute("nHistorico").toString();
         vreceituario    = (String) request.getAttribute("ntReceituario").toString();
         vexames = (String) request.getAttribute("ntExames").toString();
 
    %>
    <body>
        <%@include file="menu.jsp" %> 
        
        <header>
          <h3> Alterar este Prontuario?</h3>
        </header>
        <div> 
          <form name="form" action="update" method="post">
                     Código da Registro: <span class="badge"> <%=vregistro%></span>
                    <input type="hidden" name="registro" value="<%=vregistro%>">
                    <br/>
                    Nome: <input type="text" class="form-control" name="nome_novo"><%=vnome%><br/>
                    Data: <input type="text"  name="Data_agenda_novo"><%=vdata_agenda%><br/>
                    Historico:<textarea name="historico_novo" rows="5" cols="60" maxlength="250" ><%=vhistorico%></textarea></br>
                    Receituario:<textarea name="receituario_novo" rows="5" cols="60" maxlength="250" ><%=vreceituario%></textarea></br>
                    Exames:<textarea name="exames_novo" rows="5" cols="60" maxlength="250" ><%=vexames%></textarea></br>
                    
                   
                    <input type="submit" value="Alterar">
                </form>
            </div>
        
    </body>
</html>
