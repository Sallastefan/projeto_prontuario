drop database if exists pacientes; 
-- criaçao do banco de dados

create database pacientes;
use pacientes;
 
-- criaçao da tabela PRONTUARIO

create table PRONTUARIO (registro int primary key auto_increment ,
nome varchar(1000),
registro_agenda date,
historico varchar(1000), 
receituario varchar(1000),
exames varchar(1000));



INSERT INTO PRONTUARIO (registro,Nome,registro_agenda,historico,receituario,exames) 
values(10,'apexande','20.5.20','claustrofobia','gardenal','ultrason');
INSERT INTO PRONTUARIO (registro,Nome,registro_agenda,historico,receituario,exames) 
values(1,'Teste','19.5.20','teste','teste','teste');
INSERT INTO PRONTUARIO (registro,Nome,registro_agenda,historico,receituario,exames) 
values(2,'claudio','19.5.20','teste','nimesulida','teste');

select * from PRONTUARIO;

select  registro,Nome,historico,receituario,exames, DATE_FORMAT(registro_agenda,'%d/%m/%Y') AS registro_agenda FROM PRONTUARIO order by registro;